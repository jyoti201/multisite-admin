<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
}); */

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::Post('/getsiteid', 'SitesController@getSiteId')->name('getSiteId');
Route::Post('/webhook', 'SitesController@webhook')->name('webhook');
Route::get('/pulling/{siteid}', 'externalSettings@pulling')->name('pulling');
Route::get('/addinguser/{siteid}', 'externalSettings@addinguser')->name('addinguser');
Route::Post('/createtask', 'SitesController@createTask')->name('createTask');

Route::get('/seeder', 'CreateSeeder@index');
Route::get('/impersonate/{site}', 'SitesController@setImpersonate');

Route::resource('sites', 'SitesController');

Route::get('/users', 'UsersController@index')->name('users.index');

Route::get('/test', 'SitesController@test')->name('test');

Route::get('settings', 'SettingsController@setting')->name('settings.setting');
Route::post('settings/saveHost', 'SettingsController@saveSettings')->name('settings.saveHost');
Route::get('settings/{host}', 'externalSettings@getSetting');
