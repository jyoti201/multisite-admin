<?php
function IncludeAsset($asset){
	$theme = App\theme::where('active', 1)->where('site_id', 22)->first();
	return asset('templates/'.$theme['name'].'/'.$asset);
}
function siteSetup($host,$pass,$url){
	$data = array(
	"key" => "bAwdcKndmQkQZXbl082sbmAR6SuMaepBI9U6wt842OeBP",
	"action"=>'add',
	"domain"=> $host.".frezit.com",
	"user"=> $host,
	"pass"=> $pass,
	"email"=> 'info@frezit.com',
	"package"=> 1,
	"inode"=>10000,
	"limit_nproc"=>25,
	"limit_nofile"=>100,
	"server_ips"=> '181.215.27.70'
	);
	$url = "https://181.215.27.70:2304/v1/account";
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	curl_setopt ($ch, CURLOPT_POST, 1);
	$process = curl_exec($ch);
	curl_close($ch);
	$status = json_decode($process, true);

	if($status['status'] == "OK"){
		$db = Illuminate\Support\Facades\DB::statement('CREATE DATABASE `multisite_'.$host.'`');
		return "success";
	}else{
		return $process;
	}
}
function addWebhook($siteid){
	$site = App\Site::where('id', $siteid)->first();
	$host = $site->host_user;
	$pass = $site->host_pass;
	$url =  $site->dev_url;
	$url = $url.'/settings/update';
	$ht = array(
            'description' => 'Web integration',
            'url'  => $url,
            'active' => true,
            'events' => [
                'repo:push',
                'repo:fork',
                'repo:commit_comment_created',
                'repo:commit_status_created',
                'repo:commit_status_updated',
                'issue:created',
                'issue:updated',
                'issue:comment_created',
                'pullrequest:created',
                'pullrequest:updated',
                'pullrequest:approved',
                'pullrequest:unapproved',
                'pullrequest:fulfilled',
                'pullrequest:rejected',
                'pullrequest:comment_created',
                'pullrequest:comment_updated',
                'pullrequest:comment_deleted'
            ]
        );
	$data_string = json_encode($ht);
	$ch = curl_init();
	curl_setopt_array($ch, [
		CURLOPT_URL => "https://bitbucket.org/api/2.0/repositories/jyoti201/multisite-web/hooks",
		CURLOPT_HEADER => false,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
		CURLOPT_USERPWD => 'jyoti201:98592LAKHI#',
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_POST => true,
		CURLOPT_POSTFIELDS => $data_string
	]);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}
function pullingFiles($siteid){
	$site = App\Site::where('id', $siteid)->first();
	$host = $site->host_user;
	$url =  $site->dev_url;
	$url = $url.'/post-receive.php?user='.$host;
	$process = file_get_contents($url);
	return $process;
}
?>
