<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Task;
class PullSite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pull:site';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $task = Task::latest()->first();
		if($task){
			$site_id = $task->site_id;
			$check = explode('/', $task->url);
			if(in_array('addinguser', $check)){
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $task->url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_exec($ch);
				curl_close($ch);
				$task->delete();
			}else{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $task->url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_exec($ch);
				curl_close($ch);
				$url = str_replace('pulling', 'addinguser', $task->url);
				$task->url = $url;
				$task->save();
			}
		}
    }
}
