<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'site_title', 'user_id', 'pro_url', 'dev_url'
    ];
}
