<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Settings;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function setting()
    {
		$hostsettings = Settings::where('type', "host")->first();
		
		if($hostsettings){
			$savedsettings = unserialize($hostsettings->setting);
			return view('admin.settings', ['savedsettings'=> $savedsettings]);
		}else{
			return view('admin.settings');
		}
    }
	public function saveSettings(Request $requests)
    {
		if($requests->settingType=='host'){
			foreach($requests->all() as $key=>$request){
				$inputs[$key] = $request;
			}
			$settings = Settings::updateOrCreate(
				['type' => 'host'],
				['type' => 'host', 'setting' => serialize($inputs)]
			);
			if($settings){
				return Redirect::back()->with('success', 'Settings saved');
			}
		}
    }
}

