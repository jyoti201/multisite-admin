<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Site;
use App\User;
use App\Task;
class SitesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin.sites', ['sites' => Site::All(),'users' => User::All()]);
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
       if($request->site_title!='' && $request->dev_url!=''){
			$site = new Site;
			$host = mt_rand(10000000, 99999999);
			$url = 'http://'.$request->dev_url.'.frezit.com';
			$site->site_title = $request->site_title;
			$site->user_id = $request->user_id;
			$site->pro_url = '';
			$site->dev_url = $url;
			$site->host_user = $request->dev_url;
			$site->host_pass = $host;
			$process = siteSetup($request->dev_url,$host,$url);
			if($process=='success'){
				$site->save();
				$output['result'] = array('status'=>'success', 'message'=>'Domains Created', 'host'=> $request->dev_url);
				return json_encode($output);
			}else{
				$output['result'] = array('status'=>'fail', 'message'=>$process);
				return json_encode($output);
			}
		}else{
			$validate = array();
			if($request->site_title==''){
				$validate['error'][] = array('field'=>'site_title', 'message'=>'Site Title cannot be empty');
			}
			if($request->dev_url==''){
				$validate['error'][] = array('field'=>'dev_url', 'message'=>'Slug cannot be empty');
			}
			return json_encode($validate);
		}
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
	public function setImpersonate(Request $request)
    {
		$site = Site::where('id', $request->site)->first();
		return redirect()->to($site['dev_url'].'/authenticate/remote/'.$site['user_id'].'/'.\Config::get('app.multisiteconnectionkey'));
    }
	public function webhook()
    {
        $site = Site::latest()->first();
		$result = addWebhook($site->id);
		return $result;
    }
	public function getSiteId()
	{
		$site = Site::latest()->first();
		return $site->id;
	}
	public function createTask(Request $request){
		if($request->siteid!=''){
			$task = new Task;
			$task->site_id = $request->siteid;
			$task->url = url("/").'/pulling/'.$request->siteid;
			$task->save();
			return 'success';
		}
	}
	public function test(Request $request)
    {
		//$json = file_get_contents('https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM/crmapi&EMAIL_ID=admin@frezit.com&PASSWORD=11111111&DISPLAY_NAME=frezit');
		//$arr = explode("\n", $json);
		//$token = str_replace('AUTHTOKEN=', '', $arr[2]);
		$ht = array(
			'fromAddress'=>'test@frezit.com',
			'toAddress' => 'jbora201@gmail.com',
            'subject' => 'testing',
            'content'  => 'Email can never be dead. The most neutral and effective way, that can be used for one to many and two way communication.',
		);
		$data_string = json_encode($ht);
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => "https://mail.zoho.com/api/accounts/3183655000000152666/messages",
			CURLOPT_RETURNTRANSFER => true,
			 CURLOPT_POST => TRUE,
			 CURLOPT_HTTPHEADER => array('Content-Type: application/json;charset=UTF-8', 'Authorization: Zoho-authtoken 20b90e046abb75c05dbcec16c05e801c'),
			CURLOPT_POSTFIELDS => $data_string,
		]);
		$response = curl_exec($ch);
		dd($response);
	}
}
