<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CreateSeeder extends Controller
{
    public function Index()
    {
		exec('php artisan iseed users --force');
		exec('git push origin master');
	}
}
