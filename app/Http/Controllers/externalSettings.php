<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use Illuminate\Support\Facades\DB;
use App\Site;
use App\User;
class externalSettings extends Controller
{
    public function getSetting(Request $request){
		if($request->host){
			$hostsettings = Settings::where('type', "host")->first();
			if($hostsettings){
				$savedsettings = unserialize($hostsettings->setting);
			}
			return json_encode($savedsettings);
		}
	}
  public function pulling(Request $request)
    {
        $site = Site::where('id', $request->siteid)->first();
			$result = pullingFiles($site->id);
			if($result){
			return $result;
			}else{
			$output['result'] = array('status'=>'fail', 'message'=>'Something has gone wrong. Please inform Admin.');
			return json_encode($output);
			}
    }
	public function addinguser(Request $request)
    {
		$site = Site::where('id', $request->siteid)->first();
		$user = User::where('id', $site->user_id)->first();
		$basedomain = \Config::get('app.basedomain');
		$ht = array(
			'key'=>\Config::get('app.multisiteconnectionkey'),
			'id' => $user->id,
            'name' => $user->name,
            'email'  => $user->email,
            'pass' => $user->password,
		);
		$data_string = json_encode($ht);
		$ch = curl_init();
		curl_setopt_array($ch, [
			CURLOPT_URL => $site->dev_url."/settings/resetdb",
			CURLOPT_RETURNTRANSFER => false,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30000,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($data_string),
			CURLOPT_HTTPHEADER => array(
				// Set here requred headers
				"accept: */*",
				"accept-language: en-US,en;q=0.8",
				"content-type: application/json",
			)
		]);
		$response = curl_exec($ch);
		dd($response);
	}
}
