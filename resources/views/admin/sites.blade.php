@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style>
.buttons{
	display: none;
    padding: 0;
    margin: 0;
}
.buttons li{
	display: inline-block;
    margin-right: 10px;
    font-size: 13px;
    padding-left: 10px;
    border-left: 1px solid #cccccc;
    line-height: 1;
}
.buttons li:first-child{
	border:0;
	padding:0;
}
.table tr:hover .buttons{
	display:block;
}
#progress_report{
	background: #000;
    color: #ffffff;
    padding: 10px 20px;
	display:none;
}
</style>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<div class="btn-group pull-right">
					<button class="btn btn-default waves-effect waves-light btn-sm pull-right" data-toggle="modal" data-target="#con-close-modal">Add New Site</button>
				</div>
				<h4 class="page-title">All Sites</h4>
			</div>
			<div class="col-sm-12 m-t-15">
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Title</th>
								<th>Url</th>
								<th>Created on</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($sites) as $site)
							<tr>
								<td>
								{{ $site->site_title }}
								<ul class="buttons">
									<li><a href="">View</a></li>
									<li><a href="/impersonate/{{ $site->id }}" target="_blank">Edit</a></li>
									<li><a href="">Trash</a></li>
								</ul>
								</td>
								<td>{{ $site->dev_url }}</td>
								<td>Published<br />{{ $site->created_at }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Create New Site</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form method="POST" id="createpageform" action="">
					{{ csrf_field() }}
					<div class="col-md-12">
						<div class="form-group">
							<label for="field-1" class="control-label">Site Title</label>
							<input type="text" name="site_title" class="form-control" id="field-1" placeholder="" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="field-2" class="control-label">User</label>
							<select class="form-control" name="user_id" id="sel1">
								@foreach (json_decode($users) as $user)
									<option value="{{ $user->id }}">{{ $user->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label for="field-2" class="control-label" style="display:block;">Dashboard</label>
							<input type="text" name="dev_url" class="form-control" id="field-2" placeholder="" style="max-width: 100px;display: inline-block;">.frezit.com
						</div>
					</div>
					<!--<input type="submit" value="submit"> -->
					</form>
				</div>
				<div class="prog">
					<form id="secondstep">
					{{ csrf_field() }}
					</form>
					<div id="progress_report">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
				<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add Site</button>
			</div>
		</div>
	</div>
</div><!-- /.modal -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
<script>
$(document).ready(function(){
	$('#save').on('click', function(){
		btn = $(this);
		btn.prop('disabled', true);
		$('#progress_report').html();
		$('#progress_report').show();
		$('#progress_report').append('<p>Please wait while we are creating your site.</p>');
		$('#createpageform input[type="text"]').removeClass('parsley-error');
		$.ajax({
            type: "POST",
            url: '{{URL::route('sites.store')}}',
            data: $("#createpageform").serialize(),
            success: function( msg ) {
				var obj = jQuery.parseJSON(msg);
                if(obj.result.status == 'success'){
					$('#progress_report').append('<p>'+obj.result.message+'</p>');
					setTimeout(function(){ $('#progress_report').append('<p>Merging with Repository...</p>'); }, 300);
					$.ajax({
						type: "POST",
						url: '{{URL::route('webhook')}}',
						data: $("#secondstep").serialize(),
						success: function( msg ) {
							var obj = jQuery.parseJSON(msg);
							if(obj.uuid){
								$('#progress_report').append('<p>Successfully Merged.</p>');
								$.ajax({
									type: "POST",
									url: '{{URL::route('getSiteId')}}',
									data: $("#secondstep").serialize(),
									success: function( msg ) {
											var siteid = msg;
											$.ajax({
												type: "POST",
												url: '{{URL::route('createTask')}}',
												data: $("#secondstep").serialize()+'&siteid='+siteid,
												success: function( msg ) {
													if(msg=='success'){
													$('#progress_report').append('<p>Task generated for #Site '+siteid+'. Will take 5-15min to prepare your Dashboard</p>');
													setTimeout(function(){ window.location.href = "{{URL::route('sites.index')}}"; }, 3000);
													}
												}
											});
									}
								});


								//$('#progress_report').append('<p>Successfully Merged.</p>');
								//setTimeout(function(){ window.location.href = "{{URL::route('sites.index')}}"; }, 3000);
							}else{
								$('#progress_report').append('<p>'+msg+'</p>');
							}
						}
					});
					//window.location.href = "{{URL::route('sites.index')}}";
				}else if(obj.result.status == 'fail'){
					$('#progress_report').append('<p>'+obj.result.message+'</p>');
				}else{
					btn.prop('disabled', false);
					var obj = jQuery.parseJSON(msg);
					if(obj.error.length>0){
						for(var i = 0; i<obj.error.length; i++){
							$('input[name="'+obj.error[i].field+'"]').addClass('parsley-error');
						}
					}
				}
            }
        });
	})
	TableManageButtons.init();
})
</script>

@endsection
