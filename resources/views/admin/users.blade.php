@extends('layouts.app')

@section('extra')

<link href="{{ asset('assets/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
<style>
.buttons{
	display: none;
    padding: 0;
    margin: 0;
}
.buttons li{
	display: inline-block;
    margin-right: 10px;
    font-size: 13px;
    padding-left: 10px;
    border-left: 1px solid #cccccc;
    line-height: 1;
}
.buttons li:first-child{
	border:0;
	padding:0;
}
.table tr:hover .buttons{
	display:block;
}
</style>

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
			<div class="col-sm-12">
				<div class="btn-group pull-right">
					<button class="btn btn-default waves-effect waves-light btn-sm pull-right" data-toggle="modal" data-target="#con-close-modal">Add New User</button>
				</div>
				<h4 class="page-title">All Users</h4>
			</div>
			<div class="col-sm-12 m-t-15">	
				<div class="card-box">
					<table id="datatable-buttons" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
							</tr>
						</thead>
						<tbody>
							@foreach (json_decode($users) as $user)
							<tr>
								<td>
								{{ $user->id }}
								</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
    </div>
</div>

<div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog"> 
		<div class="modal-content"> 
			<div class="modal-header"> 
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
				<h4 class="modal-title">Create New User</h4> 
			</div> 
			<div class="modal-body"> 
				<div class="row"> 
					<div class="col-sm-12"> 
					<form id="createuserform" class="form-horizontal" method="POST" action="{{ route('register') }}">
						{{ csrf_field() }}
						<div class="form-group"> 
							<div class="col-xs-12">
								<select class="form-control" name="role" id="sel1">
									<option value="admin">Admin</option>
									<option value="site-admin">Site Admin</option>
									<option value="site-subscriber">Site Subscriber</option>
								</select>
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<div class="col-xs-12">
								<input id="name" type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name') }}" required>
							</div>
						</div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<div class="col-xs-12">
								<input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required>
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<div class="col-xs-12">
								<input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
							</div>
						</div>
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<div class="col-xs-12">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
							</div>
						</div>
					</form>
					</div>
				</div> 
			</div> 
			<div class="modal-footer"> 
				<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button> 
				<button type="button" id="save" class="btn btn-info waves-effect waves-light">Add User</button> 
			</div> 
		</div> 
	</div>
</div><!-- /.modal -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.colVis.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>
<script>
$(document).ready(function(){
	$('#save').on('click', function(){
		$('#createuserform input[type="text"]').removeClass('parsley-error');
		$('#createuserform .error').remove();
		$.ajax({
            type: "POST",
            url: "{{ route('register') }}",
            data: $("#createuserform").serialize(),
            success: function( msg ) {
				if(msg.errors){
					$.each(msg.errors,function(k,value){
                        $('input[name="'+k+'"]').addClass('parsley-error');
						$('input[name="'+k+'"]').parents('.col-xs-12').append('<span class="error">'+value+'</span>');
                    });
				}else{
					$('#createuserform').prepend('<div class="alert alert-success"><strong>Success!</strong> User Successfully created. Redirecting...</div>');
					setTimeout(function(){ window.location.href = "{{URL::route('users.index')}}"; }, 5000);
				}
            }
        });
	})
	TableManageButtons.init();
})
</script>

@endsection
