@extends('layouts.app')

@section('extra')


@endsection

@section('content')



<div class="container-fluid">
	<div class="card-box">
		<div class="row">
				<div class="col-sm-12">
					<h4 class="page-title">Settings</h4>
				</div>
		</div>
		@if (\Session::has('success'))
		<div class="row">
				<div class="col-sm-12 m-t-15">
				<div class="alert alert-success">
					{!! \Session::get('success') !!}
				</div>
				</div>
		</div>
		@endif
		<div class="row">
				<div class="col-sm-6 m-t-15">	
					
						<form method="post" action="{{ route("settings.saveHost") }}">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="type">Host:</label>
								<select class="form-control" name="type" id="type">
									<option value="git" selected>Git</option>
								</select>
							</div>
							<div class="form-group">
								<label for="user">User:</label>
								<input type="text" class="form-control" name="user" id="user" value="{!! !empty($savedsettings['user'])?$savedsettings['user']:'' !!}">
							</div>
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="text" class="form-control" name="email" id="email" value="{!! !empty($savedsettings['email'])?$savedsettings['email']:'' !!}">
							</div>
							<div class="form-group">
								<label for="org">Org:</label>
								<input type="text" class="form-control" name="org" id="org" value="{!! !empty($savedsettings['org'])?$savedsettings['org']:'' !!}">
							</div>
							<div class="form-group">
								<label for="token">Token:</label>
								<input type="text" class="form-control" name="token" id="token" value="{!! !empty($savedsettings['token'])?$savedsettings['token']:'' !!}">
							</div>
							<div class="form-group">
								<label for="token">Hubspot Api Key:</label>
								<input type="text" class="form-control" name="hubspot_api_key" id="hubspot_api_key" value="{!! !empty($savedsettings['hubspot_api_key'])?$savedsettings['hubspot_api_key']:'' !!}">
							</div>
							<button type="submit" class="btn btn-default">Update</button>
							<input type="hidden" name="settingType" value="host">
						</form>
					
				</div>
		</div>
	</div>
</div>

@endsection
